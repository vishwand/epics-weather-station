# IS Weather Station
# Spring 2020
# Purpose: When given the video feed from the Realsense camera, checks every pixel
#           for if the depth value is within a certain threshold.
#           If it outside the, highlight that pixel a color.

# Eventual Goals for Function:
#  1. Boundary between pixels out of range and in range is more "fuzzy" and not a hard line.
#      "Fuzzy" / "Drop-off" function
#  2. Instead of highlighting spaces out of threshold with colors, use pictures to create a background.

# Imported classes
import pyrealsense2 as rs  # Python Intel Realsense
import numpy as np  # Python Abstract Matrix
import cv2  # OpenCV

pipe = rs.pipeline()  # Receive information from the camera
cfg = rs.config()  # Allows for change in parameters of pipeline
# Get the appropriate configuration feed for the streaming
cfg.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)  # Depth feed
cfg.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)  # Color feed
profile = pipe.start(cfg)  # Starts the pipeline

frameWidth = pipe.wait_for_frames().get_depth_frame().width  # Width of Frame (pixels)
frameHeight = pipe.wait_for_frames().get_depth_frame().height  # Height of Frame (pixels)
threshold = .3

while True:  # infinite loop
    # Wait to start taking data until the camera starts sending values;
    # returns the first frame
    frameset = pipe.wait_for_frames()

    depth_frame = frameset.get_depth_frame()  # Retrieve the first depth frame if none return empty instance
    color_frame = frameset.get_color_frame()  # Retrieve the first color frame if none return empty instance
    # print (depth_frame.get_distance(320, 240))

    color_arr = np.asanyarray(color_frame.get_data())  # Retrieve the data from frame handle
    for x in range(frameHeight):  # gets 480 pixels horizontally
        for y in range(frameWidth):  # gets 640 pixels vertically
            # If the pixel's depth is outside the threshold, color the background white
            if depth_frame.get_distance(y, x) > threshold:
                # Blue
                color_arr[x][y][0] = 255
                # Green
                color_arr[x][y][1] = 255
                # Red
                color_arr[x][y][2] = 255

    cv2.imshow("Changed", color_arr)  #
    if cv2.waitKey(1) == ord("q"):
        break;

pipe.stop()  # Stop the pipeline feed
