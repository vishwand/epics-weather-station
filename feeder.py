"""
IS Weather Station
Spring 2020
Purpose: Mock camera data using the LaRED dataset and test mask/gesture functions.
"""

# Imported classes
import mask # IS Weather Station's Mask Class
import gesture # IS Weather Station's Gesture Class
import queue as q
import cv2
import numpy as np
import matplotlib.pyplot as plt
import struct

VALID_DEPTH_THRES = 10000
INVALID_DEPTH = -1

LENGTH = 240
WIDTH = 320

rgb_test = 'rgb_test_image.jpg'
depth_test = 'depth_test_data'

class DepthFrame:
    def __init__(self, depth_arr):
        self.data = depth_arr
    
    """
    Normalizes data so you can mask a percentage of the frame
    (aka only mask closest 10%)
    """
    def normalize(self):
        for y in range(len(self.data)):
            for x in range(len(self.data[0])):
                # Anything above this value is determined to be invalid
                if self.data[y, x] > VALID_DEPTH_THRES:
                    self.data[y, x] = INVALID_DEPTH
       
        max_dist = np.amax(self.data)
        for y in range(len(self.data)):
            for x in range(len(self.data[0])):
                if self.data[y, x] == INVALID_DEPTH:
                    continue
                
                self.data[y, x] *= 100
                self.data[y, x] /= max_dist

    def get_distance(self, y, x):
        return self.data[y, x]


"""
Masks the given percentage of the rbg file using the given depth data.
The masked image is then saved down to the given directory.
"""
def test_mask(rbg_file, depth_file, mask_percentage, output_name="mask.png", \
                                                     output_dir="mask-test", \
                                                     mask_rgb=False):
    # Prepping RGB frame
    rgb = cv2.imread(rgb_test) 
    rgb = cv2.resize(rgb, (WIDTH, LENGTH))
  
    # Unpacks binary file to a 16bit integer array, then normalizes data
    with open(depth_file, mode='rb') as f:
        depth_data = f.read()
    depth_data = struct.unpack("H" * (len(depth_data) // 2), depth_data)
    depth_data = np.reshape(depth_data, (LENGTH, WIDTH))
    depth_frame = DepthFrame(depth_data)
    depth_frame.normalize()

    # Saving new masked image
    mask_arr = mask.get(rgb, depth_frame, depth=mask_percentage, mask_rgb=mask_rgb)
    
    output_loc = output_dir + "/" + output_name
    plt.imsave(output_loc, mask_arr)

    return mask_arr

"""
Detects any gestures in a masked image, then queues said gesture.
If any gestures are detected, an image of their contour will be saved at
the given directory.
"""
def test_gestures(mask_frame, queue, output_name="gesture-contour.png", \
                                     output_dir="gesture-test"):
    g_pic = gesture.queue_gestures(mask_frame, queue)
    output_loc = output_dir + "/" + output_name 
    cv2.imwrite(output_loc, g_pic)

# Mask the closest 25% of the image, then record any gestures
gestures = q.Queue()

mask = test_mask(rgb_test, depth_test, 25)
test_gestures(mask, gestures)

gesture = gestures.get()
print("Detected gesture at X: " + str(gesture.x) + ", Y: " + str(gesture.y))
