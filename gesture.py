"""
IS Weather Station
Spring 2020
Purpose: Find the contours in an image, then queue them as a gesture.
"""

# Imported classes
import cv2
import numpy as np
import queue
import time

"""
Holds information regarding a gesture.
(Will be modified to fit requirements of final product)
"""
class Gesture:
    def __init__(self, hull, pos_y, pos_x, area, time):
        self.hull = hull 
        self.y = pos_y
        self.x = pos_x
        self.area = area
        self.time = time

"""
Queues the found gesture then returns a frame with the drawn contour.
"""
def queue_gestures(mask_frame, g_queue):
    grayscale_mask = cv2.cvtColor(cv2.UMat(mask_frame), cv2.COLOR_BGR2GRAY)
 
    ret, thresh = cv2.threshold(grayscale_mask, 127, 255, 0)
    cnts, hier = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

    gesture = choose_gesture(cnts)
    g_queue.put(gesture)
   
    return cv2.drawContours(mask_frame, [gesture.hull], -1, (0, 155, 0), 2)

"""
Finds the contour with the largest convex hull, then returns
said contour wrapped as a gesture.
"""
def choose_gesture(cnts):
    max_cnt_area = cv2.contourArea(cnts[0])
    max_cnt = cnts[0]

    for cnt in cnts:
        hull = cv2.convexHull(cnt)
        area = cv2.contourArea(hull)

        if area > max_cnt_area:
            max_cnt_area = area
            max_cnt = hull

    M = cv2.moments(max_cnt)
    cx = int(M['m10']/M['m00'])
    cy = int(M['m01']/M['m00'])

    return Gesture(max_cnt, cy, cx, max_cnt_area, time.time())
