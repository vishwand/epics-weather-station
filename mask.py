"""
IS Weather Station
Spring 2020
Purpose: Given a normalized RBG and depth frame, return a masked image
         using given filter parameters.
"""

# Imported classes
import pyrealsense2 as rs  # Python Intel Realsense
import cv2
import numpy as np

INVALID_DEPTH = -1

#BGR Values
BLACK = [0, 0, 0]
WHITE = [255, 255, 255]

def get(rgb_frame, depth_frame, depth=10.0, mask_rgb=False):
    length = len(rgb_frame)
    width = len(rgb_frame[0])

    mask = np.zeros((length, width, 3), dtype=np.uint8) 

    for y in range(length):
        for x in range(width):
            if depth_frame.get_distance(y, x) < depth and \
               depth_frame.get_distance(y, x) != INVALID_DEPTH:
                if mask_rgb:
                    mask[y, x] = rgb_frame[y, x]
                else:
                    mask[y, x] = WHITE
    
    return mask
